#utilizamos un linux ligero con nodejs instalado
FROM node:7-alpine

#creamos carpeta de destino para los packages
RUN mkdir -p /app

#definir directorio de trabajo
WORKDIR /app

#copiar package
COPY package.json /app

#instalamos el sistema de gestion de paquetes para nodejs
RUN npm install

#copiar archivos a la carpeta app
COPY . /app

#habilitar puerto
EXPOSE 3000/tcp

#inicializar el gestor de paquetes
CMD ["npm","start"] 
